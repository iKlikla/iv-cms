<?php

function install() {
	db()->query("ALTER TABLE `user_data` ADD `last_read` INT UNSIGNED NOT NULL AFTER `last_ip`;");
}

function remove() {
	db()->query("ALTER TABLE `user_data` DROP `last_read`;");
}
