<?php

// @TODO: Make this clean

$syntax = $panel['script'] == 'html' ? 'html' : 'php';
if( isset( $_POST['source'] ))
	$panelvars['content'] = $_POST['source'];

?>
<div class="box">
	<h2>Panel bearbeiten: <?php echo htmlspecialchars( $panel['name']); ?></h2>
	<div>
		<form action="<?php echo EDITOR_SELF; ?>" name="editor" method="post">
			<script language="javascript" type="text/javascript" src="assets/edit_area/edit_area_full.js"></script>
			<script language="javascript" type="text/javascript">editAreaLoader.init({ id : "source", syntax: "<?php echo $syntax; ?>", start_highlight: true, allow_toggle: false });</script>

			<textarea style=" width:100%;" rows="30" name="source" id="source"><?php echo htmlspecialchars( $panelvars['content'] ); ?></textarea>
			<p align="center">
				<input type="submit" value="Speichern" class="btn btn-primary">
				<a href="<?php echo LAYER_SELF; ?>" class="btn">Abbrechen</a>
			</p>
		</form>
	</div>
</div>
